import static spark.Spark.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import info.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.net.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.ibm.icu.text.Transliterator;


public class App {
    public static  Info info;
    public static String[] HtmlFile = {"src/main/html/template.html","src/main/html/lunch_menu.html","src/main/html/index.html"};
    public static  ObjectMapper mapper;
    public static  JsonNode root;
    public static void main(String[] args) {
        staticFiles.location("/public");
        get("/index", (req, res) -> {
            return readFile(HtmlFile[2]).replace("MenuRan",XmlParse());
        });
        Tenki();
        get("/lunch", (req, res) -> {
            String str = "";
            Boolean result = textNumber(req.queryParams("n"));                      
            if(result == false){
                str = readFile(HtmlFile[1]).replace("Text",teishoku2(req.queryParams("n"))).replace("MenuRan",XmlParse());                   
            }else if(result){
                int n = Integer.parseInt(req.queryParams("n"));
                str = readFile(HtmlFile[1]).replace("Text",teishoku(n)).replace("MenuRan",XmlParse());
            }
            return str;
        });
     
    }


    public static Boolean seiki(String str,String par){
        String par1 = par;
        String str1 = str;
        String[] moji = {"Katakana-Hiragana","Hiragana-Latin","Any-Upper"};
        boolean result = false;
        for(int a=0;a<moji.length;a++){
            Transliterator trans = Transliterator.getInstance(moji[a]);
            par1 = trans.transliterate(par1);
            str1 = trans.transliterate(str1);
        }
        Pattern p = Pattern.compile("["+ par1 +"]{"+par1.length()+",}");
        Matcher m = p.matcher(str1);      
        return m.find();
    }

    public static Boolean textNumber(String text){
        boolean result = true;
        for(int i = 0; i < text.length(); i++) {
            if(Character.isDigit(text.charAt(i))) {
            continue;
            }else {
            result =false;
            break;
            }
        }
        return result;
    }

    public static String XmlParse(){
        String name = "";
        try {
                URL url = new URL("http://weather.livedoor.com/forecast/rss/primary_area.xml");
                HttpURLConnection http = (HttpURLConnection)url.openConnection();
                http.setRequestMethod("GET");
                http.connect();
                DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = dbfactory.newDocumentBuilder();
                Document doc = builder.parse(http.getInputStream());
                Element root = doc.getDocumentElement();
                NodeList nl1 = root.getElementsByTagName("channel");
                Node nd1 = nl1.item(0);
                NodeList nl2 = ((Element)nd1).getElementsByTagName("ldWeather:source");
                Node nd2 = nl2.item(0);
                NodeList nl3 = ((Element)nd2).getElementsByTagName("pref");
                for( int i = 0; i < nl3.getLength(); i++ ) {
                    name += "<li><a href=>" + printAttribute("title",nl3.item(i)) + "&raquo;</a><ul>";                    
                    for( int a = 0; a < ((Element)nl3.item(i)).getElementsByTagName("city").getLength(); a++) {
                        name += "<li><a href=http://localhost:4567/weather?n="+ printAttribute("id",((Element)nl3.item(i)).getElementsByTagName("city").item(a))  +">" + printAttribute("title",((Element)nl3.item(i)).getElementsByTagName("city").item(a)) + "</a></li>";
                    }
                    name += "</ul></li>";
                }
                http.disconnect();
            }
            catch (Exception e) {
            }
            return name;
    }

    private static String printAttribute(String id, Node node) {
        String data = "";
        NamedNodeMap attributes = node.getAttributes();
        if (attributes!=null) {
            Node attribute = attributes.getNamedItem(id);
            data = attribute.getNodeValue();
        }
        return data;
    }

    public static String teishoku2(String n){
        jouhou();
        int b = 0;
        int c = 0;
        String f = "";
        for(int a=0;a<info.menu.size();a++){
            if(seiki(info.menu.get(a).name,n)){
                b++;
                f +="<li>" + info.menu.get(a).name + " : " + info.menu.get(a).cost + "円</li>";
            }
        }
        if(b<=0){
            f = "該当するものがありません";
        }
        return f;
    }

    public static String teishoku(int n){
        jouhou();
        int b = 0;
        int c = 0;
        String f = "";
        for(int a=0;a<info.menu.size();a++){
            if(n >= info.menu.get(a).cost){
                b++;
                f +="<li>" + info.menu.get(a).name + " : " + info.menu.get(a).cost + "円</li>";
            }
        }
        if(b<=0){
            f = "あなたが食べられる物はありません";
        }
        return f;
    }

    public static void Tenki(){
        get("/weather", (req, res) -> {
            String data = "";
            String text = "";
            try {
                String url = "http://weather.livedoor.com/forecast/webservice/json/v1?city=" + req.queryParams("n");
                String charset = "UTF-8";
                List<String> contents = read(url, charset);
                for(String str : contents) {
                    data += str;
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
            JsonNode json = null; 
            JSONArray jsonArray = null;
            try{
                ObjectMapper mapper = new ObjectMapper();
                json = mapper.readTree(data);
                jsonArray = new JSONObject(data).getJSONArray("pinpointLocations");
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
            String text1 = json.get("description").get("text").asText();
            text = json.get("title") + "<p>" + text1.replace("\n","</p><p>") + "</p>";
            String url = "";
            for(int f=0;f<jsonArray.length();f++){
                url += "<p><a href=" + json.get("pinpointLocations").get(f).get("link").asText() + ">" +  json.get("pinpointLocations").get(f).get("name").asText()+"</a></p>";
            }
            return readFile(HtmlFile[0]).replace("Title",json.get("title").asText()).replace("Text",text1.replace("\n","</p><p>")).replace("Other",url).replace("time",json.get("publicTime").asText().replace(":00+0900","")).replace("MenuRan",XmlParse());
        }); 
    }

    public static void jouhou(){
        String data = "";
        try {
            String url = "http://localhost:4567/lunch_info.json";
            String charset = "UTF-8";
            List<String> contents = read(url, charset);
            for(String str : contents) {
                data += str;
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        info = gson.fromJson(data, Info.class);        
    }

    public static List<String> read(String url,String charset) throws Exception {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            is = conn.getInputStream();
            isr = new InputStreamReader(is,charset);
            br = new BufferedReader(isr);
            ArrayList<String> lineList = new ArrayList<String>();
            String line = null;
            while((line = br.readLine()) != null) {
                lineList.add(line);
            }
            return lineList;
        }finally {
            try {
                br.close();
            }catch(Exception e) {
            }
            try {
                isr.close();
            }catch(Exception e) {
            }
            try {
                is.close();
            }catch(Exception e) {
            }
        }
    }

    public static String readFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.readString(path);
    }
}